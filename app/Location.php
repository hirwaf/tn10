<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $fillable = [
        'location',
    ];

    public function places()
    {
    	return $this->hasMany('App\Place');
    }

    public function getMyPlaces(int $id = 0){
    	if( $id > 0 && is_integer($id) ){
    		$places = $this->places->where('location_id', $id)
    						->all();
    		if(count($places) > 0 )
    			return $places;
    	}

    	return "No place registered !!";
    }

}
