<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    public function employee()
    {
    	return $this->hasMany('App\Employee');
    }

    public function canUseSystem(){
    	$array = [];
    	$get = $this->where("can",true)->get();
    	foreach ($get as $value) {
    		$array[] = $value->id;
    	}
    	return $array;
    }
}
