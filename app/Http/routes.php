<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function(){
 
    Route::get('/', function () {
        return view('welcome');
    });
    
    //Admin Route
    Route::get('/super', [
        'as'    =>  'login.super',
        'uses'  =>  'Auth\SuperController@getLogin'
    ]);
    Route::post('/super',[
        'as'    =>  'login.super',
        'uses'  =>  'Auth\SuperController@login'
    ]);
    
    //Logout
    Route::get('/super/logout',[
        'as'    =>  'logout.super',
        'uses'  =>  'Auth\SuperController@logout'
    ]);

    //Auth Routes
    Route::auth();

    //User Prefix
    Route::group(['prefix' => 'user'],function(){
        Route::get('/',[
            'as'    =>  'login.user',
            'uses'  =>  'Auth\AuthController@getLogin'
        ]);
        Route::post('/',[
            'as'    =>  'login.user',
            'uses'  =>  'Auth\AuthController@login'
        ]);
        Route::group([ 'namespace' => 'Users', 'middleware' => 'auth' ], function(){
            Route::get('/dashboard',[
                'as'    =>  'dashboard.user',
                'uses'  =>  'UserController@dashboard'
            ]);
        });

        Route::get('/logout',[
            'as'    =>  'logout.user',
            'uses'  =>  'Auth\AuthController@logout'
        ]);


    });


    //Admin Middleware Group
    Route::group(['namespace' => 'Super', 'middleware' => 'admin'],function(){

        Route::get('/dashboard', [
            'as'   => 'dashboard.super', 
            'uses' => 'SuperController@getDashboard'
        ]);
        //Users
        Route::get('/profile', [
            'as'    =>  'profile.super',
            'uses'  =>  'SuperController@getProfile'
        ]);
        Route::post('/profile', [
            'uses'    =>  'SuperController@updateProfile'
        ]);

        Route::get('/all/users', [
            'as'    =>  'all.user.super',
            'uses'  =>  'UsersController@index'
        ]);
        Route::get('/add/user', [
            'as'    =>  'add.user.super',
            'uses'  =>  'UsersController@create'
        ]);
        Route::post('/add/user', [
            'as'    =>  'save.user.super',
            'uses'  =>  'UsersController@store'
        ]);
        Route::post('all/users',[
            'as'    =>  'delete.user.super',
            'uses'  =>  'UsersController@destroy'
        ]);

        //Location
        
        Route::get('/add/place','LocationController@addLocation');
        
        Route::get('new/location',[
            'as'    =>  'new.location.super',
            'uses'  =>  'LocationController@newLocation'
        ]);

        Route::get('/add/location', [
            'as'    =>  'add.place.super',
            'uses'  =>  'LocationController@addLocation'
        ]);

        Route::get('/all/locations', [
            'as'    =>  'all.place.super',
            'uses'  =>  'LocationController@getAllLocations'
        ]);

        Route::post('/add/place',[
            'as'    =>  'save.place.super',
            'uses'  =>  'LocationController@savePlace'
        ]);

        Route::post('/add/location',[
            'as'    =>  'add.location.super',
            'uses'  =>  'LocationController@create'
        ]);

        Route::post('delete/location',[
            'as'    =>  'delete.location.super',
            'uses'  =>  'LocationController@delete'
        ]);

        Route::post('update/location', [
            'as'    =>  'update.location.super',
            'uses'  =>  'LocationController@update'
        ]);

        //Post
        Route::get('/posts',[
            'as'    =>  'employee.post.super',
            'uses'  =>  'PostsController@index'
        ]);
        Route::post('/post',[
            'as'    =>  'save.post.super',
            'uses'  =>  'PostsController@store'
        ]);
        //Employee
        Route::get('/all/employees',[
            'as'    =>  'all.employees.super',
            'uses'  =>  'EmployeeController@index'
        ]);
        Route::get('/add/employee',[
            'as'    =>  'add.employee.super',
            'uses'  =>  'EmployeeController@create'
        ]);

        Route::post('/add/employee',[
            'as'    =>  'store.employee.super',
            'uses'  =>  'EmployeeController@store'
        ]);

        Route::get('/edit/employee/{id}',[
            'as'    =>  'edit.employee.super',
            'uses'  =>  'EmployeeController@edit'
        ]);

        Route::post('/edit/employee',[
            'as'    =>  'update.employee.super',
            'uses'  =>  'EmployeeController@update'
        ]);
        Route::post('/delete/employee',[
            'as'    =>  'delete.employee.super',
            'uses'  =>  'EmployeeController@destroy'
        ]);


    });

    //Route::get('', array('https', function(){}));

    
    
});
