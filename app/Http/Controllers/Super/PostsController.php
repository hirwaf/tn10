<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use Auth;
use Session;
use Validator;

class PostsController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    protected function admin()
    {
        return Auth::guard('admins')->user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.layouts.super.posts',[
            'admin' => $this->admin()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.layouts.super.posts',[
            'admin' => $this->admin()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if(\Hash::check($request->password,$this->admin()->password)){
            $validator = Validator::make($request->all(),[
                'post'  =>  'required|min:2|unique:posts,post'
            ]);
            if( ! $validator->fails()){
                $post = new Post;
                $post->post = $request->post;
                $post->can  = ($request->can == 1 ) ? true : false;
                if($post->save()){
                    
                    Session::put('alert-success', ucfirst( $request->post ) . ' was successfuly added to job posts.' );
                    return redirect()->route('employee.post.super');

                }
                else{
                    Session::put('old',$request->all());  
                    Session::put('alert-warning', 'Server error ! Try again later. ' );
                    return redirect()->route('employee.post.super');
                }
            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first());
                return redirect()->route('employee.post.super');
            }
         }
         else{
            \Session::put('error', true);
            return redirect()->route('employee.post.super');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
