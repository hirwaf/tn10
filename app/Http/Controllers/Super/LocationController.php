<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Location;
use App\Place;

class LocationController extends Controller
{
	public $admin;

    public function __construct(){

        $this->middleware('admin');

        $this->admin = Auth::guard('admins')->user();

    }

    public function addLocation(){
        return view('auth.layouts.super.addlocation',[
            'admin' 	=> $this->admin,
            'locations'	=>	Location::all()
        ]);
    }

    public function newLocation()
    {
    	return view('auth.layouts.super.addloc',[
            'admin' 	=> $this->admin	
        ]);
    }

    public function getAllLocations(){

    	return view('auth.layouts.super.alllocations', [
    		'admin' 	=> 	$this->admin,
    		'locations' => 	Location::all(),
    		'places'	=>	Place::all()
    	]);
    }

    public function create(Request $request){
    	$password = Auth::guard('admins')->user()->password;
    	if(Hash::check($request->input('password'),$password)){	
    		$validator = Validator::make($request->all(), [
                'location' => 'required|min:4|unique:locations,location'
            ]);

    		if($validator->fails())
    		{	
    			\Session::put('alert-danger',$validator->errors()->first());
                return redirect()->route('new.location.super');
    		}
	    	else
	    	{	
	    		$location = new Location;
	    		$location->location = ucfirst($request->input('location'));
	    		$location->active = true;
	    		$location->save();

	    		\Session::put('alert-success', ucfirst( $request->input('location')) . ' was successfuly saved.' );
	            return redirect()->route('add.place.super');
	    	}
    	}
    	else{
    		\Session::put('error', true);
            return redirect()->route('new.location.super');
    	}
    }

    public function savePlace(Request $request){
    	$password = Auth::guard('admins')->user()->password;
    	if(Hash::check($request->input('password'),$password)) 
    	{	
    		$validator = Validator::make($request->all(), [
                'locationID'	=>	'required|exists:locations,id',
                'place'			=>	'required|min:3|unique:places,place',
                'description'	=>	'min:10'
            ]);

    		if($validator->fails())
    		{
    			\Session::put('alert-danger',$validator->errors()->first());
                return redirect()->route('add.place.super');
    		}
	    	else
	    	{	
	    		$place = new Place;
	    		$place->location_id = $request->input('locationID');
	    		$place->place = ucfirst($request->input('place'));
	    		$place->description = ucfirst($request->input('description'));
	    		$place->active = true;
	    		$place->save();

	    		\Session::put('alert-success', ucfirst($request->input('place')) . ' was successfuly saved.');
	            return redirect()->route('add.place.super');
	    	}
    	}
    	else
    	{
    		\Session::put('error', true);
            return redirect()->route('add.place.super');
    	}
    }

    public function delete(Request $request){
    	$password = Auth::guard('admins')->user()->password;
    	if(Hash::check($request->input('password'),$password)){
    		$location = Location::where('id', $request->input('location'));
    		$location->delete();
    		return redirect()->route('all.place.super');
    	}
    	else{
    		\Session::put('error', true);
            return redirect()->route('all.place.super');
    	}
    }

    public function update(Request $request){
    	$password = Auth::guard('admins')->user()->password;
    	if(Hash::check($request->input('password'),$password)){
    		
    		$location = Location::find($request->input('location'));
    		$location->location = $request->input('locationn');
    		$location->save();

    		return redirect()->route('all.place.super');
    	}
    	else{
    		\Session::put('error', true);
            return redirect()->route('all.place.super');
    	}
    }

}
