<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Location;
use App\Place;
use App\Post;
use Auth;
use Hash;
use Session;
use Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $admin;

    public function __cunstruct()
    {
        $this->middleware('admin');
    }

    protected function admin(){
        return Auth::guard('admins')->user();
    }

    public function index()
    {
        return view('auth.layouts.super.employees',[
            'admin'     =>  $this->admin(),
            'employees'    =>  Employee::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.layouts.super.addemployee',[
            'admin'     =>  $this->admin(),
            'places'    =>  Place::all(),
            'posts'     =>  Post::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        if(Hash::check($request->password, $this->admin()->password)){
           $phone = $request->phone;
           $phone = str_replace('+', '', str_replace(" ", "", $phone));
           $nID = str_replace(' ', '', $request->nationId);
           $input = [
                'name'      =>  $request->name,
                'email'     =>  $request->email,
                'phone'     =>  $phone,
                'nationId'  =>  $nID,
                'post'      =>  $request->post,
                'gender'    =>  $request->gender,
                'place'     =>  $request->place
           ];
           $validator = Validator::make($input,[
                'name'      =>  'required|min:6',
                'email'     =>  ($input['post'] == "Man-Power" ? '' : 'required|' ) . 'email|unique:employees,email',
                'phone'     =>  'required|integer|unique:employees,phone',
                'nationId'  =>  'required|integer|unique:employees,nationID',
                'post'      =>  'required|integer',
                'gender'    =>  'required',
                'place'     =>  'required|integer'
           ]);

           if (! $validator->fails()) {
               
               $employee = new Employee;
               $employee->names     = ucfirst($input['name']);
               $employee->email     = $input['email'];
               $employee->phone     = $input['phone'];
               $employee->nationID  = $input['nationId'];
               $employee->post_id   = ucfirst($input['post']);
               $employee->gender    = ucfirst($input['gender']);
               $employee->place_id  = $input['place'];
               
               if($employee->save()){
                Session::put('alert-success', ucfirst( $request->name ) . ' was successfuly added.' );
                return redirect()->route('add.employee.super');
               }
               else{
                Session::put('old',$request->all());    
                Session::put('alert-warning', 'Server Error !' );
                return redirect()->route('add.employee.super');
               }
           }
           else{
            Session::put('old',$request->all());
            Session::put('alert-danger',$validator->errors()->first());
            return redirect()->route('add.employee.super');
           }

        }
        else{
            Session::put('error', true);
            Session::put('old',$request->all());
            return redirect(route('add.employee.super'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }
}
