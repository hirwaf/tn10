<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
Use Auth;
use App\User;
use Session;
use Validator;


class UsersController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('admin');
    }

    protected function admin()
    {
        return Auth::guard('admins')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.layouts.super.alluser',[
            'admin' =>  $this->admin(),
            'users' =>  User::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.layouts.super.adduser',[
            'admin'     =>  $this->admin(),
            'employees' =>  \App\Employee::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(\Hash::check($request->password,$this->admin()->password)){
            $id = $request->name;
            $employee = Employee::find($id);
            $password = trim($request->makepassword);
            $users = User::all();
            $exists = false;
            
            foreach ($users as $user) {
                if( $employee->email == $user->email ){
                    $exists = true;
                    break;
                }
            }

            if($exists){
                Session::put('alert-warning', '" '.ucfirst($employee->names).' " Already Added !' );
                return redirect()->route('add.user.super');
            }
            elseif( $password != "" AND isset($password) AND strlen($password) >= 8 ){
                $user = new User();
                $user->email = $employee->email;
                $user->telephone = $employee->phone;
                $user->employee_id = $id;
                $user->password = bcrypt($password);
                if($user->save()){
                    Session::put('alert-success', ucfirst( $request->name ) . ' was successfuly added to your employees.' );
                    Session::put('userS',[
                        'name'      =>  $employee->names,
                        'email'     =>  $employee->email,
                        'phone'     =>  $employee->phone,
                        'password'  =>  $password
                    ]);
                    return redirect()->route('add.user.super');                
                }
                else{
                    Session::put('old',$request->all());  
                    Session::put('alert-warning', 'Server error ! Try again later. ' );
                    return redirect()->route('add.user.super');
                }
            }else{
                Session::put('old',$request->all());
                Session::put('alert-danger', 'Password must be 8 characters or more ! ' );
                return redirect()->route('add.user.super');
            }

        }else{
            \Session::put('error', true);
            return redirect()->route('add.user.super');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(\Hash::check($request->password,$this->admin()->password)){
            $user = User::find($request->user);
            $what = $request->what;
            $user->allowed = $what;
            $user->save();
            return redirect()->route('all.user.super');
        }else{
            \Session::put('error', true);
            return redirect()->route('all.user.super');
        }
    }
}
