<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Admin;
use App\Place;
use App\Location;

class SuperController extends Controller
{
    //
    public $admin;
    
    public function __construct(){
        $this->middleware('admin');
        
        $this->admin = Auth::guard('admins')->user();
    }
    
    public function getDashboard(){
        
        return view('auth.layouts.super.dashboard',[
            'admin' => $this->admin
        ]);
        
    }
    
    public function getProfile(){
        return view('auth.layouts.super.profile',[
            'admin' =>  $this->admin
        ]);
    }
    
    public function updateProfile(Request $request){
        $password = $this->admin->password;
        if(Hash::check($request->input('password'),$password)){
            
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' =>  'required|email'.($this->admin->email == $request->input('email'))? '' : '|unique:admins,email',
                'phone' =>  'required|min:12'.($this->admin->email == $request->input('email'))? '' : '|unique:admins,phone'
            ]);
            if($request->has('new_password') AND $request->has('new_password_confirmation') ){
                $inp = [
                    'new_password'               =>  $request->get('new_password'),
                    'new_password_confirmation'  =>  $request->get('new_password_confirmation')
                ];
                $validator = Validator::make($inp,[
                    'new_password'   =>  'required|confirmed|min:8'
                ]);
            }
            if( !$validator->fails() ){
                
                $admin = Admin::findOrFail($this->admin->id);
                $admin->name = $request->input('name');
                $admin->email = $request->input('email');
                $admin->phone = $request->input('phone');
                if(isset($inp)){
                    $admin->password = bcrypt($request->input('new_password'));
                }
                $admin->save();

                \Session::put('alert-success','Your profile has successfuly update .');
                return redirect()->route('profile.super');
            }
            else{
                \Session::put('alert-danger',$validator->errors()->first());
                return redirect()->route('profile.super');
            }
        }
        else{
            \Session::put('error', true);
            return redirect()->route('profile.super');
        }
    }

    

}
