<?php

namespace App\Http\Controllers\Users;

use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
	protected $guard = "web";

	public function __construct()
	{
		$this->middleware('auth');
		if(Auth::check()){
			$user = Auth::user()->id;
			$user = \User::find($user);
			$user->active = true;
			$user->save();
		}
	}

	public function dashboard(){
		echo "User dashboard <a href='".url(route('logout.user'))."'>Logout</a> ";
	}
}
