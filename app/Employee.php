<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
   	protected $fillable = [
        'names',
        'email',
        'phone',
        'nationID',
        'post',
        'gender'
    ];

    public function place(){
    	return $this->belongsTo('App\Place');
    }

    public function user(){
    	return $this->hasOne('App\User');
    }

    public function post(){
    	return $this->belongsTo('App\Post');
    }

}
