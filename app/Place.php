<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    //
	protected $fillable = [
        'place',
    ];

    public function location()
    {
    	return $this->belongsTo('App\Location');
    }

    public function employee()
    {
    	return $this->hasOne('App\Employee');
    }
}
