<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('telephone')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('isanew')->default(true);
            $table->boolean('active')->default(false);
            $table->boolean('allowed')->default(true);
            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')
                  ->references('id')
                  ->on('employees');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
