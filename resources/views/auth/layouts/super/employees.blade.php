<?php  
  $errorP = false;
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
?>
@extends('layouts.app_admin')
@section('title',"All Locations  | $admin->name ")
@section('style_p')
  <link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset(url('plugins/datatables/dataTables.bootstrap.css')) }}">
@endsection
@section('styles')
<style type="text/css">
  .on{
    text-align: left;
    background: #16A085;
    color: #fff;
    cursor: none;
  }
  .on .fa ,.off .fa{
    padding-top: .25em; 
  }
  .on:hover{
    color: #fff;
  }
  .off:click{
    box-shadow: none;
  }
  .off{
    text-align: right;
    cursor: none;
    color: #fff;
    background-color: #34495E; 
  }
</style>
@endsection
@section('menu')
    @include('auth.layouts.super.menu')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Employees
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Employees</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="box">
            <div class="box-body">
              <table id="example2" class="table table-hover">
                <thead>
                <tr>
                  <th>Names</th>
                  <th>Email</th>
                  <th>Telephone</th>
                  <th>Location</th>
                  <th>Post</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  @foreach($employees as $employee)
                    <tr>
                      <td style="cursor: pointer;text-transform:capitalize;">{{ ucfirst($employee->names) }}</td>
                      <td>{{ $employee->email }}</td>
                      <td>+{{ $employee->phone}}</td>
                      <td><?= "<label  class='label label-info' >" . $employee->place->location->location. "</label> <label class='label label-info' >" . $employee->place->place ."</label>" ?></td>
                      <!-- Post -->
                      <td><label class="label label-default">{{ $employee->post->post }}</label></td>
                      <!-- Block & Unblock section -->
                      <td><button class='btn btn-sm btn-block btn-danger block' value='".$i."' >Delete</button></td>
                    </tr>
                    <?php modalDelete($i++,$employee->names,$employee->id); ?>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Names</th>
                    <th>Email</th>
                    <th>Telephone</th>
                    <th>Location</th>
                    <th>Post</th>
                    <th></th>
                  </tr>
                </tfoot>
              </table>              
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@section('script_p')
  <!-- DataTables -->
  <script src="{{ asset(url('plugins/datatables/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(url('plugins/datatables/dataTables.bootstrap.min.js')) }}"></script>
  <!-- FastClick -->
  <script src="{{ asset(url('plugins/fastclick/fastclick.js')) }}"></script>
@endsection
@section('script')
  <script type="text/javascript">
    $(function(){
      $('.bs-example-modal-sm').modal();

      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
      $('.block').on('click',function(e){
        var b = e.target.value;
        $("*#modalTitle").text('Block');
        $('.bs-modal-'+b).modal();
        $("*#what").attr('value',function(){
          return "0";
        });
      });
      $('.unblock').on('click',function(e){
        var b = e.target.value;
        $("*#modalTitle").text('Unblock');
        $('.bs-modal-'+b).modal();
        $("*#what").attr('value',function(){
          return "1";
        });
      });
    });
  </script>
@endsection
@section('scripts_top')
@endsection
<?php
function modalDelete($i,$title,$id){
?>
<div class="modal modal-primary fade modal-top-20 bs-modal-{{ $i }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">
          <span id="modalTitle"></span>&nbsp;
          <span style="text-transform: uppercase;">{{ $title }}</span> 
        </h4>
      </div>
      <div class="modal-body">
          <form action="{{ url(route('delete.user.super')) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name='what' style="color:#444;" id='what' >
            <input type="hidden" name="user" value="{{ $id }}" >
            <div class="form-group">
              <input required type="password" focus='true' name='password' class="form-control border-none radius-2 passwordS" id="inputPassword3" placeholder="Password">
              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password and press ENTER, to <span id="modalTitle" style="text-transform: lowercase;" ></span> this user for using the system.</span>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<?php  
}
?>