<?php  
  $errorP = false;
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
?>
@extends('layouts.app_admin')
@section('title',"New Location | $admin->name")
@section('menu')
    @include('auth.layouts.super.menu')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add New Place
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Location</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-md-9 col-md-offset-1">
            <div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" role="form" method="post" action='{{ url(route("save.place.super")) }}' >
            {{ csrf_field() }}
              <div class="box-body">
                <div class="clearfix">&nbsp;</div>
                <div class="flash-message">
                    <?php $mm = ''; ?>
                    @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                        <?php
                            if( Session::has('alert-' . $msg) ){
                                $mm = 'alert-'.$msg;
                                $m = Session::get('alert-' . $msg);
                                Session::forget('alert-' . $msg);
                            }
                        ?>
                        @if( $mm == ('alert-'.$msg) AND !Session::has('loc') )
                            <p class="alert alert-{{ $msg }}">
                                {{ $m }}
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
                <div class="form-group">
                  <label for="loc" class="col-sm-2 control-label">Location *</label>
                  <div class="col-sm-10">
                    <select name="locationID" required class="form-control select2"  title ="Select in existing location">
                      <option selected="selected" disabled value="" ></option>
                      @if(count( $locations) > 0 )
                        @foreach( $locations as $location )
                          <option value=" {{ $location->id }} " >{{ $location->location }}</option>
                        @endforeach
                      @else
                        <option selected="selected" disabled value="" >No location is avaible</option>
                      @endif
                    </select>
                  </div>
                </div>
                <div class="form-group">
                 <div class="col-sm-10 col-sm-offset-2">
                   <button type="button" class="btn btn-md btn-link btn-sm add-loc" title="Add new location if is not registered">Add New Location</button>
                 </div> 
                </div>
                <div class="form-group">
                  <label for="place" class="col-sm-2 control-label">Place *</label>
                  <div class="col-sm-10">
                    <input required type="text" id='place' name="place" class="form-control"  placeholder="Place">
                  </div>
                </div>
                <div class="form-group">
                  <label for="desc" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                   <textarea class="textarea" name='description' placeholder="Place some description here or leave it empty" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                </div>
                <div class='clearfix'>&nbsp;</div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="col-sm-7 col-sm-offset-3">
                  <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
                  <span class="help-block info" style='font-size: x-small;' >Please fill in your password, to save changes.</span>
                </div>
                <div class="clearfix"></div>
                <button type="submit" class="btn  btn-info  btn-md col-sm-3 col-md-offset-5">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
  @if ( $errorP )
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
          </div>
          <div class="modal-body">
              <h3>Incorrect Password !!</h3>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  @endif
@endsection
@section('script_p')
<!-- Select2 -->
<script src="{{ asset(url('plugins/select2/select2.full.min.js')) }}"></script>
@endsection
@section('script')
<script type="text/javascript">
  $(function(){
    $(".select2").select2();
  });
</script>
@endsection
@section('style_p')
<!-- Select2 -->
  <link rel="stylesheet" href="{{ asset(url('plugins/select2/select2.min.css')) }}">
@endsection
@section('scripts_top')
<script type="text/javascript">
    $(function(){
        $('.bs-example-modal-sm').modal();
        $('.add-loc').on('click',function(){
          $('.new-loc-modal').modal();
        });
    });
</script> 
@endsection
<div class="modal fade new-loc-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Register New Location</h4>
      </div>
      <div class="modal-body">
      <form class="form" role='form' action="{{ url(route('add.location.super')) }}" method="post">
      {{ csrf_field() }}
      <div class="box box-primary">
      <div class="clearfix">&nbsp;</div>
        <div class="flash-message">
            <?php 
              $mm = '';
              $loc = false;
              $loc = Session::has('loc') == true ? Session::has('loc') : $loc ;
              Session::forget('loc'); 
            ?>
            @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                <?php
                    if( Session::has('alert-' . $msg) ){
                        $mm = 'alert-'.$msg;
                        $m = Session::get('alert-' . $msg);
                        Session::forget('alert-' . $msg);
                    }
                ?>
                @if( $mm == ('alert-'.$msg) AND $loc )
                    <p class="alert alert-{{ $msg }}">
                        {{ $m[0] }}
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </p>
                @endif
            @endforeach
        </div>
        <div class="box-body">
          <div class="form-group">
            <label for="lo" class="col-sm-2 control-label">Location</label>
            <div class="col-sm-10">
              <input required type="text" id='lo' name="location" class="form-control"  placeholder="Location" title='Add new location ' >
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="clearfix"></div>

        </div>
        <div class="box-footer">
          <div class="col-sm-7 col-sm-offset-3">
            <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
            <span class="help-block info" style='font-size: x-small;' >Please fill in your password, to save changes.</span>
          </div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" ><i class="fa fa-save"></i> Save</button>
        </form>
      </div>
    </div>
  </div>
</div>