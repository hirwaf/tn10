<?php  
  $errorP = false;
  $olds = "";
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
  if(Session::has('old')){
      $olds = Session::get('old'); 
      Session::forget('old');
  }
  function olds($olds,$name){
    if($olds != "")
      return $olds[$name];
  }
?>
@extends('layouts.app_admin')
@section('title',"Add New Employee | $admin->name ")
@section('menu')
    @include('auth.layouts.super.menu')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add New Employee
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Employees</li>
        <li class="active">New Employee</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-md-9 col-md-offset-1">
            <div class="box box-primary">
            <!-- form start -->
            <form class="form-horizontal" role="form" method="post" action='{{ url(route("store.employee.super")) }}' >
            {{ csrf_field() }}
              <div class="box-body">
                <div class="clearfix">&nbsp;</div>
                <div class="flash-message">
                    <?php $mm = ''; ?>
                    @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                        <?php
                            if( Session::has('alert-' . $msg) ){
                                $mm = 'alert-'.$msg;
                                $m = Session::get('alert-' . $msg);
                                Session::forget('alert-' . $msg);
                            }
                        ?>
                        @if( $mm == ('alert-'.$msg) )
                            <p class="alert alert-{{ $msg }}">
                                {{ $m }}
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Names</label>
                  <div class="col-sm-10">
                    <input required type="text" id='name' name="name" class="form-control" id="name" value="{{ olds($olds,'name') }}" placeholder="Peter elix">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" name="email" id="email" value="{{ olds($olds,'email') }}" placeholder="example@domain.com">
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone</label>  
                  <div class="col-sm-10">
                      <div class="input-group">
                          <input required type="text" class="form-control" name="phone" id="phone" data-inputmask='"mask": "+25# ### ### ###" ' data-mask value="{{ olds($olds,'phone') }}" placeholder="+250 788 300 222">
                          <div class="input-group-addon">
                              <i class="fa fa-phone"></i>
                          </div>
                      </div>   
                  </div>
                </div>
                <div class="form-group">
                  <label for="nId" class="col-sm-2 control-label">Nation ID</label>  
                  <div class="col-sm-10">
                    <input required type="text" class="form-control" name="nationId" id="nationalID" value="{{ olds($olds,'nationId') }}" data-inputmask='"mask": "# #### # ####### # ##" ' data-mask  placeholder="1 1999 8 00123374 1 54">
                  </div>
                </div>
                <div class="form-group">
                  <label for="post" class="col-sm-2 control-label" >Post</label>
                  <div class="col-sm-4">
                    <select class="form-control" name='post' >
                      <option value="" disabled="true" {{ olds($olds,'post') == "" ? "selected" : ""  }}> Choose from the list </option>
                      @foreach($posts as $post)
                        <option value="{{ $post->id }}" {{ olds($olds,'post') == $post->id ? "selected":"" }}>{{ ucfirst($post->post) }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="gender" class="col-sm-2 control-label">Gender</label>  
                  <div class="col-sm-4">
                      <select id="gender" name="gender" required class="form-control" >
                        <option value="male" {{ olds($olds,'gender') == 'female' ? "":"selected" }} selected> Male </option>
                        <option value="female" {{ olds($olds,'gender') == 'female' ? "selected":"" }} > Female </option>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="place" class="col-sm-2 control-label">Location</label>  
                  <div class="col-sm-4">
                    <select id='place' name="place" required class="form-control">
                      <option value="" disabled selected > Where to work</option>
                      <option disabled>&nbsp;</option>
                      @foreach($places as $place)
                        <option value="{{ $place->id }}" {{ olds($olds,'place') == $place->id ? "selected":"" }} > {{ $place->place }} </option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="col-sm-7 col-sm-offset-3">
                  <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
                  <span class="help-block info" style='font-size: x-small;' >Please fill in your password, to save changes.</span>
                </div>
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-info btn-md col-sm-3 col-md-offset-5">Add user</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@endsection
@section('scripts_top')
  <script type="text/javascript">
    $(function(){
      $('.bs-example-modal-sm').modal();
    });
  </script>
@endsection