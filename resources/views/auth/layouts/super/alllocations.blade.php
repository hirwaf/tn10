<?php  
  $errorP = false;
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
?>
@extends('layouts.app_admin')
@section('title',"All Locations  | $admin->name ")
@section('styles')
<link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
@endsection

@section('menu')
    @include('auth.layouts.super.menu')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Locations
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Location</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="box">
            <div class="box-body">
              <div class="box-group" id="accordion">
            <?php $i = 0;?>
                @foreach($locations as $location)
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-{{ $location->active == 1 ? 'navy' : 'danger' }}">
                  <div class="box-header with-border">
                    <h5 class="box-title" style='cursor:pointer;color:#16A085;width: 80%;' data-toggle="collapse" data-parent="#accordion" href="#collapse{{ ++$i }}" >
                        {{ $location->location }}
                    </h5>
                    <button class="btn btn-link pull-right btn-sm" id="deleteBtn" value="{{ $i }}" >Delete</button>
                    <button class="btn btn-link pull-right btn-sm" id="editBtn" value="{{ $i }}" >Edit</button>
                  </div>
                  <div id="collapse{{ $i }}" class="panel-collapse collapse">
                    <div class="box-body">
                      @if(is_array($location->getMyPlaces($location->id)))
                        <table class="table table-condensed">
                          <?php $o = 1 ?>
                          @foreach($location->getMyplaces($location->id) as $place )
                            <tr>
                              <td width="20px">{{ $o. '.' }}</td>
                              <td>{{ $place->place }}</td>
                              <td>{{ $place->description }}</td>
                            </tr>
                            <?php $o ++; ?>
                          @endforeach
                        </table>
                      @else
                        <div>
                          {{ $location->getMyPlaces($location->id) }}
                        </div>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="modal modal-primary fade modal-top-20 bs-modal-{{ $i }}" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel2">Delete <span style="text-transform: uppercase;">{{ $location->location }}</span> </h4>
                      </div>
                      <div class="modal-body">
                          <form action="{{ url(route('delete.location.super')) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="location" value="{{ $location->id }}" >
                            <div class="form-group">
                              <input required type="password" name='password' class="form-control border-none radius-2" id="inputPassword3" placeholder="Password">
                              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password and press ENTER, to delete this location.</span>
                            </div>
                          </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal modal-primary fade modal-top-20 bs-modal-update-{{ $i }}" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel2">Update <span style="text-transform: uppercase;">{{ $location->location }}</span> </h4>
                      </div>
                      <div class="modal-body">
                          <form action="{{ url(route('update.location.super')) }}" class="form-{{ $i }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="location" value="{{ $location->id }}" >
                            <div class="form-group">
                              <label for="input1">Location</label>
                              <input required type="text" name='locationn' class="form-control border-none radius-2" id="input1" value="{{ $location->location }}">
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="form-group">
                              <input required type="password" name='password' class="form-control border-none radius-2" id="inputPassword3" placeholder="Password">
                              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password ,to edit this location.</span>
                            </div>
                          </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" value='{{ $i }}' class="btn btn-md btn-primary pull-right btn-sub"  >Update</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.example-modal -->                
              @endforeach
              </div>
            <!-- /.box-body -->
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@section('scripts')
<!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
@endsection
@section('scripts_top')
  <script type="text/javascript">
    $(function(){
      function trim(string) {
        return string.replace(/^\s+|\s+$/gm,'');
      }
      $('.bs-example-modal-sm').modal();
      $("* #deleteBtn").click(function(e){
        var t = e.target.value;
        $('.bs-modal-' + t).modal();
      });
      $("* #editBtn").click(function(e){
        var v = e.target.value;
        $('.bs-modal-update-' + v).modal();
      });

      $("*.btn-sub").on('click',function(e) {
        var v = e.target.value;
        var i = trim( $('.form-'+ v +' #input1').val() ).length;
        var l = trim( $('.form-'+ v +' #inputPassword3').val() ).length;
        if( i <= 3 && i != "" )
          alert("The location field must be at minimum of 3 characters ! ");
        if( i <= 3 && i == "" )
          alert("The location field is required ! ");
        else if(l <= 0 )
          alert('The password field is required ! ');
        else{
           $('.form-'+ v).submit();
        }
      });

    });
  </script>
@endsection