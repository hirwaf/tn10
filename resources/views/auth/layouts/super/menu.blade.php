<?php
    function activeMenu($values){
        $route = Route::current();
        $nameRoute = $route->getName();
        for($x = 0;$x < count($values);$x++) {

        	if($values[$x] == $nameRoute)
            	return "active";
        }
        return "";
    }
    $profile = activeMenu( ['profile.super'] );
    $workers = activeMenu( array('all.user.super','add.user.super','all.employees.super','add.employee.super','employee.post.super') );
    $place = activeMenu( ['add.place.super','all.place.super','new.location.super'] ); 
    $users = activeMenu(['all.user.super','add.user.super']);
    $loan = "";
    $products = "";
    $stock = "";
?>

<li class=" {{ $stock }} treeview">
    <a href="#">
        <i class="fa  fa-file-text-o"></i> <span>Report</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">

        <li><a href="{{ url(route('add.user.super')) }}"><i class="fa fa-money"></i> Sales </a></li>
        <li><a href="{{ url(route('all.user.super')) }}"><i class="fa fa-hourglass-2"></i> Stock </a></li>        
        <li><a href="{{ url(route('add.user.super')) }}"><i class="fa fa-beer"></i> Expenses </a></li>
        <li><a href="{{ url(route('add.user.super')) }}"><i class="fa fa-bank"></i> Net-Profit </a></li>
    </ul>
</li>
<li class=" {{ $products }} treeview">
    <a href="#">
        <i class="fa  fa-th"></i> <span>Products</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ url(route('all.place.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Items</a></li>
        <li><a href="{{ url(route('add.place.super')) }}"><i class="fa  fa-plus-circle"></i> New Item</a></li>
        <li>
            <a href="#">
                <i class="fa  fa-list"></i> <span>Model</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ url(route('all.user.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Models</a></li>
                <li><a href="{{ url(route('all.user.super')) }}"><i class="fa fa-plus-circle"></i> New Model</a></li>
            </ul>
        </li>
    </ul>
</li>
<li class=" {{ $loan }} treeview">
    <a href="#">
        <i class="fa fa-credit-card"></i> <span>Credits</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">

        <li><a href="{{ url(route('all.user.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Credits</a></li>
        <li><a href="{{ url(route('add.user.super')) }}"><i class="fa fa-paypal"></i> Payed Credits </a></li>
        <li><a href="{{ url(route('add.user.super')) }}"><i class="fa fa-plus-circle"></i> New Credit </a></li>
    </ul>
</li>

<li class=" {{ $place }} treeview">
    <a href="#">
        <i class="fa fa-location-arrow"></i> <span>Location</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">

        <li><a href="{{ url(route('all.place.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Places</a></li>
        <li><a href="{{ url(route('add.place.super')) }}"><i class="fa  fa-plus-circle"></i> New Place</a></li>
    </ul>
</li>

<li class=" {{ $workers }} treeview">
    <a href="#">
        <i class="fa fa-industry"></i> <span>Employee</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">

        <li><a href="{{ url(route('all.employees.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Employees</a></li>
        <li><a href="{{ url(route('add.employee.super')) }}"><i class="fa fa-plus-circle"></i> Add Employee</a></li>
        <li class="{{ $users }}">
            <a href="#">
                <i class="fa fa-users"></i> <span>Users</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ url(route('all.user.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Users</a></li>
                <li><a href="{{ url(route('add.user.super')) }}"><i class="fa fa-user-plus"></i> Add User</a></li>            
            </ul>
        </li>
        <li><a href="{{ url(route('employee.post.super')) }}"><i class="fa fa-sticky-note-o"></i>Posts</a></li>
    </ul>
</li>
<li class=" {{ $profile }} treeview" ><a href="{{ url(route('profile.super')) }}"><i class="fa fa-user"></i> <span>My Profile</span></a></li>