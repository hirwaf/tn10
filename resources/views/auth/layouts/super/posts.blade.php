<?php  
  $errorP = false;
  $userS = false;
  $olds = "";
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
  if(Session::has('old')){
      $olds = Session::get('old'); 
      Session::forget('old');
  }
  function olds($olds,$name){
    if($olds != "")
      return $olds[$name];
  }
?>
@extends('layouts.app_admin')
@section('title',"Add New User | $admin->name")
@section('menu')
    @include('auth.layouts.super.menu')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Posts
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Employee</li>
        <li class="active">Posts</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-9 col-md-offset-1">
            <div class="box box-primary">
              <!-- form start -->
              <form role="form" action="{{ url(route('save.post.super')) }}" method="post" >
              {{ csrf_field() }}
                <div class="box-body">
                <div class="flash-message">
                  <?php $mm = ''; ?>
                  @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                      <?php
                          if( Session::has('alert-' . $msg) ){
                              $mm = 'alert-'.$msg;
                              $m = Session::get('alert-' . $msg);
                              Session::forget('alert-' . $msg);
                          }
                      ?>
                      @if( $mm == ('alert-'.$msg))
                          <p class="alert alert-{{ $msg }}">
                              {{ $m }}
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          </p>
                      @endif
                  @endforeach
                </div>
                <div class="clearfix">&nbsp;</div>
                  <div class="form-group">
                      <label for="post" class="col-sm-2 control-label">Post</label>
                      <div class="col-sm-10">
                        <input required type="text" id='post' name="post" class="form-control"  placeholder="Post" title='Add new post ' value="{{ olds($olds,'post') }}" >
                      </div>
                  </div>
                  <div class="form-group">
                   <div class="col-sm-10 col-sm-offset-2">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="can" value="1"> This Post can interact with the system
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="box-footer">
                  <div class="col-sm-7 col-sm-offset-3">
                    <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
                    <span class="help-block info" style='font-size: x-small;' >Please fill in your password, to save changes.</span>
                  </div>
                  <div class="clearfix">&nbsp;</div>
                  <button type="submit" class="btn  btn-info  btn-md col-sm-3 col-md-offset-5">Save</button>
                </div>
              </form>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="box box-info">
            <!-- table start -->
            </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@if ( $userS )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" style="padding-top: 2em;" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">{{ ucfirst($userS['name']) }} Successfuly Added</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered">
              <tr>
                <td>Email</td>
                <td style="background-color: #2C3E50;">&nbsp;</td>
                <td>{{ $userS['email'] }}</td>
              </tr>
              <tr>
                <td>Telephone</td>
                <td style="background-color: #2C3E50;">&nbsp;</td>
                <td>+{{ $userS['phone'] }}</td>
              </tr>
              <tr>
                <td>Password</td>
                <td style="background-color: #2C3E50;">&nbsp;</td>
                <td>{{ $userS['password'] }}</td>
              </tr>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@endsection
@section('style_p')
<link rel="stylesheet" href="{{ asset(url('plugins/select2/select2.min.css')) }}">
@endsection
@section('style')
<style type="text/css">
  .invisible{
    visibility: hidden;
  }
</style>
@endsection
@section('script_p')
<script src="{{ asset(url('plugins/select2/select2.full.min.js')) }}"></script>
@endsection
@section('scripts_top')
<script type="text/javascript">
  $(function(){
    $('.bs-example-modal-sm').modal();
    //$('.make-password').hide();
  });
</script>
@endsection
@section('script')
<script type="text/javascript">
  $(function(){
    $(".select2").select2();
  });
</script>
@endsection